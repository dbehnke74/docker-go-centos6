FROM centos:6
RUN yum -y update && \
  yum -y install curl gcc gcc-c++ libaio-devel tar gzip git && \
  yum clean all && \
  rm -r -f /var/cache/yum/*
COPY getgo.sh /opt/
RUN cd /opt && chmod +x ./getgo.sh && ./getgo.sh && mkdir /usr/local/go
RUN groupadd -g 1000 somebody && useradd -m -u 1000 -g 1000 somebody && chown -R somebody:somebody /usr/local/go
ENV GOROOT=/opt/go
ENV GOPATH=/usr/local/go
ENV PATH=/opt/go/bin:/usr/local/go/bin:$PATH